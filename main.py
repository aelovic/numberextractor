from PIL import Image
import glob

def load_files(directory_path):
    image_list = []
    for filename in glob.glob('{0}/*.jpeg'.format(directory_path)):
        im = Image.open(filename)
        image_list.append(im)
    return image_list


def extract_number_frames(image_list):
    return []


def execute_ocr(image):
    ""
    return ""


def retrieve_numbers_by_ocr(frames):
    result = []
    for frame in frames:
        result.append(execute_ocr(frame))
    return result

def load_binah_results(files_path):
    return ""


def save_diff_with_binah_results(results, binah_results):
    # returns a file with final results
    return ""

def main():
    image_list = load_files("/Users/assafelovic/Desktop/edan")
    frames = extract_number_frames(image_list)
    numbers = retrieve_numbers_by_ocr(frames)
    binah_results = load_binah_results("FILE_PATH")
    save_diff_with_binah_results(numbers, binah_results)

main()




